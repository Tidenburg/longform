(function(){
  var maxTick = 50; // Ticks before deletion starts
  var chunkLength = 100; // Length before safe.

  // A timer to delete text if they take too long.
  const tickTimer = window.setInterval(doTick, 100);
  var ticks = maxTick;

  // HTML components
  var writeHistory = document.querySelector(".write__history"); // Will be erased after timeout
  var writeSafe = document.querySelector(".write__safe");       // Is safe from being erased (happens after chunkLength characters are written)
  var centerWord = document.querySelector(".center-word");
  var progressBarInner = document.querySelector(".progress-bar__inner");
  var titleInput = document.querySelector(".title-input");
  var tutorialPopup = document.querySelector(".tutorial-text");
  var settingsPopup = document.querySelector(".settings");
  var downloadButton = document.querySelector(".download-button");
  var titleEditButton = document.querySelector(".title-edit");
  var settingsEditButton = document.querySelector(".settings-edit");
  var settingsBlockSizeMenu = document.querySelector(".block-size-menu");
  var settingsEraseDelayMenu = document.querySelector(".erase-delay-menu");

  var docTitle = "";

  function doTick(){
    // If they're out of time, remove a character.
    if(--ticks <= 0){
      writeHistory.innerHTML = writeHistory.innerHTML.substring(0, writeHistory.innerHTML.length - 1);    
    }

    // Remove the tutorial as they begin to type
    if(writeHistory.innerHTML.length + writeSafe.innerHTML.length + centerWord.innerHTML.length == 0){
      tutorialPopup.style.animation = "fade-in 2s forwards";
    }

    redrawProgressBar();
  }

  function redrawProgressBar(){
    progressBarInner.style.width = (writeHistory.innerHTML.length / chunkLength * 100) + "%";
  }

  // Check for settings changes
  function refreshSettings(){
    chunkLength = settingsBlockSizeMenu.getAttribute("data-value");
    maxTick = settingsEraseDelayMenu.getAttribute("data-value") * 10;

    redrawProgressBar();
  }

  // Downloads the current written text as a .txt file with the document title name
  function downloadText() {
    var element = document.createElement('a');
    var dateStamp = new Date().toLocaleString();
    var allText = writeSafe.innerHTML + writeHistory.innerHTML;
    var fileTitle = ((docTitle != "") ? docTitle : "Longform Doc");

    // Create a link to a 'data' element with the encoded text
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(allText));
    element.setAttribute('download', fileTitle + " " + dateStamp);

    // Insert hidden dummy element
    element.style.display = 'none';
    document.body.appendChild(element);
    
    // Trigger a click on the dummy to save
    element.click();
    
    document.body.removeChild(element);
  }

  //  Shows the settings pane.
  settingsEditButton.addEventListener("click", function(){
    if(settingsPopup.style.display != "none"){
      settingsPopup.style.display = 'none';
    } else {
      settingsPopup.style.display = "block";
    }
  });

  // Toggles the edit title textbox.
  titleEditButton.addEventListener("click", function(){
    if(titleInput.style.display != "none"){
      titleInput.style.display = 'none';
    } else {
      titleInput.style.display = "inline-block";
    }
  });

  // Starts the process to save the document.
  downloadButton.addEventListener("click", function(){ 

    if(docTitle == ""){
      // Hide the input field and save the title if one has been entered, otherwise prompt to enter a title
      if(titleInput.value != ""){
        titleInput.style.display = "none";
        docTitle = titleInput.value;
        downloadText();

      } else {
        titleInput.style.display = "inline-block";
        titleInput.focus();
      }

    } else {
      titleInput.style.display = "none";

      // Download the current text.
      downloadText(); 
    }
  });

  // Confirmation text before closing the window.
  /*window.addEventListener("beforeunload", function (e) {
    var confirmationMessage = "\o/";

    e.returnValue = confirmationMessage;     // Gecko, Trident, Chrome 34+
    return confirmationMessage;              // Gecko, WebKit, Chrome <34
  });/*/

  document.addEventListener('DOMContentLoaded', function () {

    // Add the current keypress into the center of the screen, if a space or enter is pressed, add it to the body
    //    text.
    document.onkeypress = function(evt) {
        evt = evt || window.event;
        var charCode = evt.keyCode || evt.which;
        var charStr = String.fromCharCode(charCode);

        if(document.activeElement != titleInput){

          // If they press space or return, add the new word in the screen-center to the text body.
          if(charStr == " " || charStr == "\r"){
            writeHistory.innerHTML += centerWord.innerHTML + charStr;
            centerWord.innerHTML = "";

            // Scroll to the bottom
            window.scrollTo(0, document.body.scrollHeight);

            // Reset the delete timer.
            ticks = maxTick;
          } else {
            centerWord.innerHTML += charStr;
          }

          // If they've written enough content, move it to the 'safe' zone.
          if(writeHistory.innerHTML.length >= chunkLength){
            writeSafe.innerHTML += writeHistory.innerHTML;
            writeHistory.innerHTML = "";
          }

          // Remove the tutorial as they begin to type
          if(writeHistory.innerHTML.length + writeSafe.innerHTML.length + centerWord.innerHTML.length > 0){
            tutorialPopup.style.animation = "fade-out 2s forwards";
          }

          redrawProgressBar();

          // Prevent <space> page scroll
          evt.preventDefault();
        } else {
          docTitle = titleInput.value + charStr;
        }
    };

    // Detect backspace press because keypress event won't fire.)
    document.onkeydown = function(evt) {
      evt = evt || window.event;
        var charCode = evt.keyCode || evt.which;
        var charStr = String.fromCharCode(charCode);

        // Remove the character.
        if(charCode == 8){
          centerWord.innerHTML = centerWord.innerHTML.substring(0, centerWord.innerHTML.length - 1);    
        }
    }

  });

  // A hackish button-group implementation
  document.querySelectorAll('.menu-bar--group > .button').forEach(function(element){
    element.addEventListener("click", function(e){
      var siblings = element.parentElement.getElementsByClassName('button');

      for(var i = 0; i < siblings.length; i++){
        siblings[i].classList.remove('selected');
      }

      element.classList.add('selected');
      element.parentElement.setAttribute("data-value", element.textContent);

      refreshSettings();
    });
  });

})();






















